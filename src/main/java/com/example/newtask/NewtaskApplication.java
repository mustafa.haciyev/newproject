package com.example.newtask;

import com.example.newtask.dto.UserRequestDto;
import com.example.newtask.model.User;
import com.example.newtask.repo.UserRepository;
import com.example.newtask.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@RequiredArgsConstructor
public class NewtaskApplication implements CommandLineRunner {

    private final UserRepository userRepository;

    public static void main(String[] args) {
        SpringApplication.run(NewtaskApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {

        userRepository.save(User.builder()
                .id(1L)
                .name("Mustafa")
                .email("Mustafa@gmail.com")
                .password("12345")
                .build());
    }
}
