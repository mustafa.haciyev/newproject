package com.example.newtask.dto;

import com.example.newtask.unique.Unique;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotEmpty;
import lombok.*;
import lombok.experimental.FieldDefaults;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class UserRequestDto {

    @NotEmpty(message = "Name is required")
    String name;
    @NotEmpty(message = "Email is required")
    @Email(message = "Invalid email format")
    @Unique(message = "Email must be unique")
    String email;
    @NotEmpty(message = "Password is required")
    String password;


}
