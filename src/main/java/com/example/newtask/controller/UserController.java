package com.example.newtask.controller;

import com.example.newtask.dto.LoginUserRequestDto;
import com.example.newtask.dto.UserRequestDto;
import com.example.newtask.service.UserService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/users")
public class UserController {
    private final UserService userService;

    @PostMapping
    public Long saveUser(@Valid @RequestBody UserRequestDto userRequestDto){
        if (!userService.isEmailUnique(userRequestDto.getEmail())){
            throw new RuntimeException("Email is already taken");
        }
       return userService.saveUser(userRequestDto);
    }

    @PostMapping("/login")
    public String loginUser(@Valid @RequestBody LoginUserRequestDto loginUserRequestDto){
       return userService.loginUser(loginUserRequestDto);
    }



}
