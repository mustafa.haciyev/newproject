package com.example.newtask.unique;

import com.example.newtask.service.UserService;
import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class UniqueValueValidator implements ConstraintValidator<Unique,String> {

    private final UserService userService;
    @Override
    public void initialize(Unique constraintAnnotation) {

    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        return userService.isEmailUnique(value);    }
}
