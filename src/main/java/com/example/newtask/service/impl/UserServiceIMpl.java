package com.example.newtask.service.impl;

import com.example.newtask.config.AppConfiguration;
import com.example.newtask.dto.LoginUserRequestDto;
import com.example.newtask.dto.UserRequestDto;
import com.example.newtask.exception.EmailExistsException;
import com.example.newtask.exception.PasswordIncorrectException;
import com.example.newtask.exception.UserNotFOundException;
import com.example.newtask.model.User;
import com.example.newtask.repo.UserRepository;
import com.example.newtask.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class UserServiceIMpl implements UserService {

    private final UserRepository userRepository;
    private final AppConfiguration appConfiguration;


    @Override
    public Long saveUser(UserRequestDto userRequestDto) {
        if (userRepository.existsByEmail(userRequestDto.getEmail())){
            throw new EmailExistsException("Email already exists");
        }
        User user = appConfiguration.getMapper().map(userRequestDto, User.class);
        return userRepository.save(user).getId();
    }

    @Override
    public String loginUser(LoginUserRequestDto loginUserRequestDto) {

        Optional<User> optionalUser = userRepository.findByEmail(loginUserRequestDto.getEmail());

        if (optionalUser.isEmpty()){
            throw new UserNotFOundException("User not found!");
        }
        User user = optionalUser.get();

        if ( !user.getPassword().equals(loginUserRequestDto.getPassword())){
              throw new PasswordIncorrectException("Password is incorrect!");
          }
        return "Succesfully logged in!";
    }

    @Override
    public boolean isEmailUnique(String email) {
        return !userRepository.existsByEmail(email);
    }
}


//        optionalUser.ifPresent(user -> {
//          if ( !user.getPassword().equals(loginUserRequestDto.getPassword())){
//              throw new RuntimeException("Password is incorrect!");
//          }
//        }
//        );
//        return "Succesfully!";