package com.example.newtask.service;

import com.example.newtask.dto.LoginUserRequestDto;
import com.example.newtask.dto.UserRequestDto;

public interface UserService {
    Long saveUser(UserRequestDto userRequestDto);

    String loginUser(LoginUserRequestDto loginUserRequestDto);

    boolean isEmailUnique(String email);

}
